package Control;

import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private static BufferedWriter writer;
    private static SimpleDateFormat formatter;
    private static File file;

    public static void start() throws IOException {
        file = new File("logs.txt");
        if(!file.exists()) { file.createNewFile(); }
        writer = new BufferedWriter(new FileWriter("logs.txt", true));
        formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }

    public static void write(String log) {
        Date date = new Date();
        String content = "[" + formatter.format(date) + "] " + log + "\n";
        try {
            writer.append(content);
            writer.flush();
            System.out.println("Write attempt done");
        } catch (IOException e) {
            System.out.println("Failed to write a log to file");
            e.printStackTrace();
        }
    }

    public static boolean openLogFile() {
        /*Runtime cmd = Runtime.getRuntime();
        try {
            System.out.println("Opening file attempt");
            cmd.exec(path);
            System.out.println("After attempt");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }*/
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().edit(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
