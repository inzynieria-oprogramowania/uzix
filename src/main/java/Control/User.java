package Control;

import java.util.concurrent.Semaphore;

public class User {
    private static Semaphore inUse = new Semaphore(1);
    private static String username = "";
    private static String email = "";

    public static String getUsername() {
        try {
            inUse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            return username;
        } finally {
            inUse.release();
        }
    }

    public static void setUsername(String username) {
        try {
            inUse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            User.username = username;
        } finally {
            inUse.release();
        }
    }

    public static String getEmail() {
        try {
            inUse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            return email;
        } finally {
            inUse.release();
        }
    }

    public static void setEmail(String email) {
        try {
            inUse.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            User.email = email;
        } finally {
            inUse.release();
        }
    }
}
