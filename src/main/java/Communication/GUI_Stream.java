package Communication;

import Exceptions.NoContentException;
import Exceptions.NoMessageException;
import Exceptions.StreamNotReservedException;

import java.util.concurrent.Semaphore;

public class GUI_Stream {
    private Message message;
    private Semaphore reserved = new Semaphore(1);
    private Semaphore inUse = new Semaphore(1);

    public GUI_Stream() {
        message = null;
    }

    public void acquire() {
        try {
            reserved.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void release() throws StreamNotReservedException {
        if(reserved.availablePermits() > 0)
            throw new StreamNotReservedException("Stream was not reserved");
        else reserved.release();
    }

    public void send(Message message) throws NoContentException, StreamNotReservedException {
        if(reserved.availablePermits() > 0)
            throw new StreamNotReservedException("Stream was not reserved");
        else{
            try {
                inUse.acquire();
                if(message.content.size() == 0)
                    throw new NoContentException("Message sent by " + message.sender + " had no content.");
                else this.message = message;
                inUse.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Message receive() throws NoMessageException, StreamNotReservedException {
        if(reserved.availablePermits() > 0)
            throw new StreamNotReservedException("Stream was not reserved");
        else {
            try {
                inUse.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                if (message == null)
                    throw new NoMessageException("There was no message in GUI Stream");
                else return message;
            } finally {
                inUse.release();
            }
        }
    }
}
