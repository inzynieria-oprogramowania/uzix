package Communication;

import java.util.ArrayList;

public class QueryBuilder {
    private ArrayList<String> message;
    private int iterator = 0; // aktualne miejsce w kolekcji message w którym znajduje sie program po stworzeniu kwerendy zostaje wyzerowana
    public QueryBuilder(ArrayList<String> message) {
        this.message = message;
    }

    public String select() {
        StringBuilder tmp = new StringBuilder(message.get(iterator) + " ");
        iterator += 2;
        while (!message.get(iterator).equals("FROM")) {
            tmp.append(message.get(iterator - 1)).append(", ");
            iterator++;
        }
        tmp.append(message.get(iterator - 1));
        return tmp.toString();
    }
    public String from() {
        StringBuilder tmp = new StringBuilder(message.get(iterator) + " ");
        iterator +=2;
        while(iterator != message.size() && !message.get(iterator).equals("WHERE")
                && !message.get(iterator).equals("GROUP BY")
                && !message.get(iterator).equals("ORDER BY"))
        {
            tmp.append(message.get(iterator - 1)).append(", ");
            iterator++;
        }
        tmp.append(message.get(iterator - 1));
        return tmp.toString();
    }
    public String where()
    {
        StringBuilder tmp = new StringBuilder(message.get(iterator) + " ");
        iterator +=2;
        while(iterator != message.size() && !message.get(iterator).equals("ORDER BY") && !message.get(iterator).equals("GROUP BY"))
        {
            tmp.append(message.get(iterator - 1)).append(" ");
            iterator++;
        }
        tmp.append(message.get(iterator - 1));
        return tmp.toString();
    }

    /* methoda tworzace string dla ORDER BY oraz GROUP BY
      Parametr wejściowy until wyznacza do jakiej wartości string methoda ma kntynuować działanie
      */
    public String orderConditions(String until)
    {
        StringBuilder tmp = new StringBuilder(message.get(iterator) + " ");
        iterator +=2;
        while(iterator != message.size() && !message.get(iterator).equals(until))
        {
            if(message.get(iterator).equals("AND") || message.get(iterator).equals("OR")) {
                tmp.append(message.get(iterator - 1)).append(" ");
            }
            else if(message.get(iterator -1).equals("AND") || message.get(iterator -1).equals("OR")){
                tmp.append(message.get(iterator - 1)).append(" ");
            }
            else{
                tmp.append(message.get(iterator - 1)).append(", ");
            }
            iterator++;
        }
        tmp.append(message.get(iterator - 1));
        return tmp.toString();
    }

    /* wybiera która methoda powinna być następna (w zależności od kolejności występującej w danych wejściowych
       jeśli nie ma danych wejściowych zwraca null
     */
    public String nextCondition()
    {
        if(iterator >= message.size()-1)
        {
            return null;
        }
        else if(message.get(iterator).equals("WHERE"))
        {
            return where();
        }
        else if(message.get(iterator).equals("GROUP BY"))
        {
            return orderConditions("ORDER BY");
        }
        else if(message.get(iterator).equals("ORDER BY"))
        {
            return orderConditions("GROUP BY");
        }
        return null;
    }


    public String createCopyOfTable() //NOWA
    {
        StringBuilder tmp;
        if(message.get(iterator).equals("CREATE TABLE")) {

            tmp = new StringBuilder(message.get(iterator++) + " ");
        }
        else {
            tmp = new StringBuilder(message.get(iterator++) + " " + message.get(iterator++) + " ");
        }
        tmp.append(message.get(iterator++)).append(" ");
        if(!message.get(iterator).equals("AS"))
        {
            return null;
        }
        tmp.append(message.get(iterator++)).append(" (");
        String temp;
        tmp.append(select()).append(" ").append(from());
        while((temp = nextCondition()) != null)
            tmp.append(" ").append(temp);
        return tmp + ")";

    }
    public String chooseInsertInto() //NOWA
    {
        String result;
        if(message.contains("SELECT"))
        {
            result = insertIntoSelect();
        }
        else if(message.contains("VALUES")){
            result = insertInto();
        }
        else {
            result = null;
        }
        iterator = 0;
        return result;
    }

    public String chooseCreateTable() //NOWA
    {
        String result;
        if(message.contains("SELECT"))
        {
            result = createCopyOfTable();
        }
        else {
            result = null;
        }
        iterator = 0;
        return result;
    }

    public String chooseSelect() //NOWA
    {
        StringBuilder result;
        String tmp;
        if(message.contains("FROM"))
        {
            result = new StringBuilder(select() + " " + from());
            while((tmp = nextCondition()) != null)
                result.append(" ").append(tmp);
        }
        else {
            result = null;
        }
        iterator = 0;
        if(result != null)
            return result.toString();
        else
            return null;
    }

    public String delete() //NOWA
    {
        String tmp;
        tmp = message.get(iterator++) + " " + message.get(iterator++) + " " + message.get(iterator++);
        return tmp;
    }

    public String chooseDelete() //NOWA
    {
        StringBuilder result;
        String tmp;
        if(message.contains("FROM"))
        {
            result = new StringBuilder(delete());
            while((tmp = nextCondition()) != null)
                result.append(" ").append(tmp);
        }
        else {
            result = null;
        }
        iterator = 0;
        if(result != null)
            return result.toString();
        else
            return null;
    }

    public String insertIntoSelect() //NOWA
    {
        StringBuilder tmp;
        if(message.get(iterator).equals("INSERT INTO")) {

            tmp = new StringBuilder(message.get(iterator++) + " ");
        }
        else {
            tmp = new StringBuilder(message.get(iterator++) + " " + message.get(iterator++) + " ");
        }
        tmp.append(message.get(iterator++)).append(" ");
        if(!message.get(iterator).equals("SELECT"))
        {
            tmp.append("(");
            iterator++;
            while(!message.get(iterator).equals("SELECT"))
            {
                tmp.append(message.get(iterator - 1)).append(", ");
                iterator++;
            }
            tmp.append(message.get(iterator - 1)).append(") ");
        }
        tmp.append(message.get(iterator++)).append(" ");
        String temp;
        tmp.append(select()).append(" ").append(from());
        while((temp = nextCondition()) != null)
            tmp.append(" ").append(temp);
        return tmp.toString();

    }

    //wstawianie rekordów

    public String insertInto() //NOWA
    {
        StringBuilder tmp;
        if(message.get(iterator).equals("INSERT INTO")) {

            tmp = new StringBuilder(message.get(iterator++) + " ");
        }
        else {
            tmp = new StringBuilder(message.get(iterator++) + " " + message.get(iterator++) + " ");
        }
        tmp.append(message.get(iterator++)).append(" ");
        if(!message.get(iterator).equals("VALUES"))
        {
            tmp.append("(");
            iterator++;
            while(!message.get(iterator).equals("VALUES"))
            {
                tmp.append(message.get(iterator - 1)).append(", ");
                iterator++;
            }
            tmp.append(message.get(iterator - 1)).append(") ");
        }
        tmp.append(message.get(iterator++)).append(" (");
        while(iterator != message.size()-1)
        {
            tmp.append(message.get(iterator)).append(", ");
            iterator++;
        }
        tmp.append(message.get(iterator)).append(")");
        return tmp.toString();
    }
    public String chooseUpdate() //NOWA
    {
        StringBuilder result;
        String tmp;
        if(message.contains("SET"))
        {
            result = new StringBuilder(update());
            while((tmp = nextCondition()) != null)
                result.append(" ").append(tmp);
        }
        else {
            result = null;
        }
        iterator = 0;
        if(result != null)
            return result.toString();
        else
            return null;
    }

    public String update() //NOWA
    {
        StringBuilder tmp = new StringBuilder(message.get(iterator++) + " " + message.get(iterator++));
        int counter = 0;
        if(!message.get(iterator).equals("SET"))
        {
            return null;
        }
        tmp.append(" ").append(message.get(iterator++)).append(" ");
        while (counter != message.size() && !message.get(counter).equals("WHERE"))
        {
            counter++;
        }
        if((counter-3)%2!=0)
        {
            return null;
        }
        if((iterator+=2)>=message.size())
        {
            return null;
        }
        while (iterator != message.size() && !message.get(iterator).equals("WHERE"))
        {
            tmp.append(message.get(iterator - 2)).append(" = ").append(message.get(iterator - 1)).append(", ");
            iterator+=2;
        }
        tmp.append(message.get(iterator - 2)).append(" = ").append(message.get(iterator - 1));
        return tmp.toString();
    }

    // składa kwerende w zalezności od pierwzego elementu kolekcji message
    public String createQuery() { //NOWA
        String result = null;
        switch (message.get(0))
        {
            case "SELECT":
                result = chooseSelect();
                break;
            case "INSERT":
            case "INSERT INTO":
                result = chooseInsertInto();
                break;
            case "DELETE":
                result = chooseDelete();
                break;
            case "UPDATE":
                result = chooseUpdate();
                break;
            case "CREATE TABLE":
            case "CREATE":
                result = chooseCreateTable();
                break;
        }
        if(result != null)
            result += ";";
        return  result;
    }
}
