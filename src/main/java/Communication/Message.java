package Communication;

import java.util.ArrayList;

public class Message {
    public String sender;
    public String receiver;
    public ArrayList<String> content;

    public Message() {
        content = new ArrayList<>();
    }
}
