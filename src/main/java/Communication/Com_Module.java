package Communication;

import Exceptions.InitializationException;
import Exceptions.NoContentException;
import Exceptions.NoMessageException;
import Exceptions.StreamNotReservedException;
import Listeners.GUI_Listener;

import java.sql.*;

public class Com_Module extends Thread {
    private Connection con;
    private Statement stmt;
    private ResultSet rs;
    private GUI_Stream gui_stream;
    private boolean request_present;
    private boolean is_alive;
    private GUI_Listener listener;

    public Com_Module() {
        request_present = false;
        is_alive = false;
    }

    // connecting with data base as user
    public boolean connect() {
        try {
            Message message = gui_stream.receive();
            String address = "jdbc:mysql://localhost:3306/uzix";
            String login = message.content.get(0);
            String password = message.content.get(1);
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(address, login, password);
            stmt = con.createStatement();
            is_alive = true;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // passing gui stream reference
    public void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getName());
        else gui_stream = stream;
    }

    // passing gui listener reference
    public void pass_GUI_Listener(GUI_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as GUI_Listener in class " + this.getName());
        else this.listener = listener;
    }

    // Disconnect from database
    public void disconnect() {
        try {
            stmt.close();
            con.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        is_alive = false;
    }

    public void receive_request() {
        //System.out.println("Request accepted1");
        request_present = true; }

    // Continuous Com_Module working loop
    public void run() {
        while(true) {
            System.out.print("");
            if(request_present) {
                //System.out.println("Request accepted2");
                try {
                    //System.out.println(gui_stream == null ? "Null" : "Not Null");
                    //System.out.println("Is alive ? " + is_alive);
                    //System.out.println(gui_stream.receive().sender);
                    if(!is_alive && gui_stream.receive().sender.equalsIgnoreCase("LoginPanel")) {
                        //System.out.println("Login attempt");
                        if(connect()) {
                            System.out.println("Connected");
                            Message message = new Message();
                            message.sender = "Com_Module";
                            message.receiver = "LoginPanel";
                            message.content.add("OK");
                            //System.out.println("Message created");
                            gui_stream.send(message);
                            //System.out.println("Message sent");
                            listener.inform_GUI("LoginFrame");
                            //System.out.println("Information sent");
                            request_present = false;
                            //System.out.println("Request present: " + request_present);
                        } else {
                            System.out.println("Connection error");
                            Message message = new Message();
                            message.sender = this.getName();
                            message.receiver = "LoginPanel";
                            message.content.add("Error");
                            gui_stream.send(message);
                            listener.inform_GUI("LoginFrame");
                            request_present = false;
                        }
                    } else {
                        if(gui_stream.receive().sender.equalsIgnoreCase("ToolbarPanel")) {
                            disconnect();
                            request_present = false;
                        } else {
                            try {
                                // TODO: Wstawic uruchomienie query-builder
                                String query = "";
                                rs = stmt.executeQuery(query);
                                // TODO: Konwersja rs na Massage
                                // TODO: Przesłanie Message do GUI_Stream
                                listener.inform_GUI("MainFrame");
                            } catch (SQLException throwable) {
                                throwable.printStackTrace();
                            }
                            request_present = false;
                        }
                    }
                } catch (NoMessageException | StreamNotReservedException | NoContentException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
