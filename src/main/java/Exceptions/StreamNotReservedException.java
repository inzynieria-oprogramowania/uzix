package Exceptions;

public class StreamNotReservedException extends Exception {
    // constructor accepting message
    public StreamNotReservedException(String message) { super(message); }
}
