package Exceptions;

public class NoContentException extends Exception {
    // constructor accepting message
    public NoContentException(String message) {super(message); }
}
