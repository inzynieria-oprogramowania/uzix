package Exceptions;

public class NoMessageException extends Exception {
    // constructor accepting message
    public NoMessageException(String message) {super(message); }
}
