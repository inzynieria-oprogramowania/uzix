package Exceptions;

public class InitializationException extends Exception {
    // constructor accepting message
    public InitializationException(String message) {super(message); }
}