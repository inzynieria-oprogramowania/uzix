package GUI.Windows;

import Communication.GUI_Stream;
import Control.Log;
import Exceptions.InitializationException;
import GUI.Panels.LoginPanel;
import Listeners.Com_Listener;
import Listeners.Frame_Listener;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class LoginFrame extends JFrame {
    private Com_Listener com_listener;
    private Frame_Listener frame_listener;
    private GUI_Stream gui_stream;
    private LoginPanel panel;

    // pass window name to constructor
    public LoginFrame(String name) { super(name); }

    // pass com listener reference
    public void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener=listener;
    }

    // pass gui stream reference
    public void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    // pass frame listener reference
    public void pass_LF_Listener(Frame_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Frame_Listener in class " + this.getClass().getName());
        else frame_listener = listener;
    }

    // initialize window
    public void initialize() throws InitializationException {

        panel = new LoginPanel();
        // configure panel
        try {
            Log.write("Passed Com_Listener to LoginPanel");
            panel.pass_Com_Listener(com_listener);
            Log.write("Passed GUI_Stream to LoginPanel");
            panel.pass_GUI_Stream(gui_stream);
            Log.write("Passed Frame_Listener to LoginPanel");
            panel.pass_LF_Listener(frame_listener);
            Log.write("Login Panel initialization");
            panel.initialize();
            add(panel);
            // set window dimensions, position and default exit operation
            setSize(400, 110);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        } catch(InitializationException e) {
            Log.write("Error initializing LoginPanel");
            Log.write(Arrays.toString(e.getStackTrace()));
            throw e;
        }
    }

    // pass information
    public void inform() {
        panel.inform();
        Log.write("Signal passed");
    }
}
