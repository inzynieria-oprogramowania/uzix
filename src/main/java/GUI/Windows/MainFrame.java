package GUI.Windows;

import Communication.GUI_Stream;
import Control.Log;
import Exceptions.InitializationException;
import GUI.PanelType;
import GUI.Panels.General.MenuPanel;
import GUI.Panels.Menus.*;
import GUI.Panels.ToolbarPanel;
import GUI.Panels.General.WindowPanel;
import GUI.Panels.Windows.*;
import Listeners.Com_Listener;
import Listeners.Frame_Listener;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private Frame_Listener frame_listener;
    private MenuPanel menuPanel;
    private ToolbarPanel toolbarPanel;
    private WindowPanel windowPanel;
    private Com_Listener com_listener;
    private GUI_Stream gui_stream;

    // pass window name to constructor
    public MainFrame(String name) { super(name); }

    // pass com listener reference
    public void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else this.com_listener = listener;
    }

    // pass gui stream reference
    public void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else this.gui_stream = stream;
    }

    // pass frame listener reference
    public void pass_MF_Listener(Frame_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Frame_Listener in class " + this.getClass().getName());
        else this.frame_listener = listener;
    }

    // change panels accordingly
    public void update_Panels(PanelType window_panel, PanelType menu_panel) {
        // TODO: zaimplementować aktualizację paneli
        Log.write("Change panels to " + window_panel + " and " + menu_panel);
        // create new window panels
        switch(window_panel) {
            case DefaultPanel:
                this.windowPanel = new DefaultPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case CoworkersListPanel:
                this.windowPanel = new CoworkersListPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case EditInventoryPanel:
                this.windowPanel = new EditInventoryPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case ListInventoryPanel:
                this.windowPanel = new ListInventoryPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case InventoryReportPanel:
                this.windowPanel = new InventoryReportPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case OrderReportPanel:
                this.windowPanel = new OrderReportPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case ProfileEditProfilePanel:
                this.windowPanel = new ProfileEditProfilePanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case ProfileInfoPanel:
                this.windowPanel = new ProfileInfoPanel();
                Log.write("Panel " + window_panel + " setup");
                this.windowPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
        }

        // create new menu panels
        switch(menu_panel) {
            case MainMenu:
                this.menuPanel = new MainMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case AdminMenu:
                this.menuPanel = new AdminMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case CoworkersMenu:
                this.menuPanel = new CoworkersMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case InventoryMenu:
                this.menuPanel = new InventoryMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case OrdersMenu:
                this.menuPanel = new OrdersMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case ProfileMenu:
                this.menuPanel = new ProfileMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
            case ReportsMenu:
                this.menuPanel = new ReportsMenu();
                Log.write("Panel " + menu_panel + " setup");
                this.menuPanel.setup(com_listener, gui_stream, this::update_Panels);
                Log.write("Setup successful");
                break;
        }

        // refresh JFrame content pane
        Log.write("Change initialized");
        getContentPane().removeAll();
        getContentPane().add(this.windowPanel, BorderLayout.CENTER);
        getContentPane().add(this.menuPanel, BorderLayout.WEST);
        getContentPane().add(this.toolbarPanel, BorderLayout.NORTH);
        revalidate();
        Log.write("Change successful");
    }

    // inform panel that request answer is ready
    public void inform() {
        windowPanel.inform();
        Log.write("Signal passed");
    }

    public void initialize() {
        // configuring window
        setLayout(new BorderLayout());
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(dim.width,dim.height);
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // initializing panels
        menuPanel = new MainMenu();
        menuPanel.setup(com_listener, gui_stream, this::update_Panels);
        add(menuPanel, BorderLayout.WEST);

        windowPanel = new DefaultPanel();
        windowPanel.setup(com_listener, gui_stream, this::update_Panels);
        add(windowPanel, BorderLayout.CENTER);

        toolbarPanel = new ToolbarPanel();
        toolbarPanel.setup(com_listener, gui_stream, this::update_Panels);
        add(toolbarPanel, BorderLayout.NORTH);
    }
}
