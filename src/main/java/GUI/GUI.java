package GUI;

import Communication.GUI_Stream;
import Control.Log;
import Exceptions.InitializationException;
import GUI.Windows.LoginFrame;
import GUI.Windows.MainFrame;
import Listeners.Com_Listener;

import java.awt.*;
import java.util.Arrays;


public class GUI {
    private Com_Listener com_listener;
    private MainFrame main_frame;
    private LoginFrame login_frame;
    private GUI_Stream gui_stream;

    // set listener to signal for communication module
    public void set_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener = listener;
    }

    // set gui_stream to exchange information with communication module
    public void set_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    public void initialize() throws InitializationException {
        login_frame = new LoginFrame("UZIX v 0.1");
        login_frame.setVisible(true);
        Log.write("Login_Frame created");
        try {
            login_frame.pass_Com_Listener(com_listener);
            Log.write("Passed Com_Listener to LoginFrame");
            login_frame.pass_GUI_Stream(gui_stream);
            Log.write("Passed GUI_Stream to LoginFrame");
            login_frame.pass_LF_Listener(this::swap);
            Log.write("Passed FrameListener to LoginFrame");
            login_frame.initialize();
            Log.write("LoginFrame initialized");
        } catch (InitializationException e) {
            Log.write("Error initializing LoginFrame");
            Log.write(Arrays.toString(e.getStackTrace()));
            throw e;
        }
    }

    public void inform(String target) {
        if(target.equalsIgnoreCase("LoginFrame")) {
            login_frame.inform();
            Log.write("Signal passed to LoginFrame");
        } else {
            main_frame.inform();
            Log.write("Signal passed to MainFrame");
        }
    }

    private void swap() {
        // if login_frame is present on screen
        if(login_frame.isShowing()) {
            login_frame.setVisible(false);
            Log.write("Switching from LoginFrame to MainFrame");
            // initialize new main_frame
            main_frame = new MainFrame("UZIX v 0.2");
            main_frame.setVisible(true);
            Log.write("New MainFrame created");
            try {
                main_frame.pass_Com_Listener(com_listener);
                Log.write("Passed Com_Listener to MainFrame");
                main_frame.pass_GUI_Stream(gui_stream);
                Log.write("Passed GUI_Stream to MainFrame");
                main_frame.pass_MF_Listener(this::swap);
                Log.write("Passed Frame_Listener to MainFrame");
                main_frame.initialize();
                Log.write("MainFrame initialized");
            } catch (InitializationException e) {
                Log.write("Error initializing MainFrame");
                Log.write(Arrays.toString(e.getStackTrace()));
            }
        // if main_frame is present on screen
        } else {
            main_frame.setVisible(false);
            Log.write("Switching from MainFrame to LoginFrame");
            // initialize new login_screen
            login_frame = new LoginFrame("UZIX v 0.2");
            login_frame.setVisible(true);
            Log.write("New LoginFrame created");
            try {
                login_frame.pass_Com_Listener(com_listener);
                Log.write("Passed Com_Listener to LoginFrame");
                login_frame.pass_GUI_Stream(gui_stream);
                Log.write("Passed GUI_Stream to LoginFrame");
                login_frame.pass_LF_Listener(this::swap);
                Log.write("Passed Frame_Listener to LoginFrame");
                login_frame.initialize();
                Log.write("LoginFrame initialized");
            } catch (InitializationException e) {
                Log.write("Error initializing LoginFrame");
                Log.write(Arrays.toString(e.getStackTrace()));
            }
        }
    }

    // formatting function for elements of GridBagLayout
    public static void formatLocation(GridBagConstraints gbc, int gridx, int gridy, double weightx, double weighty,
                                      String method) {
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.weightx = weightx;
        gbc.weighty = weighty;
        if(method.equalsIgnoreCase("LINE_END")) gbc.anchor = GridBagConstraints.LINE_END;
        else if(method.equalsIgnoreCase("LINE_START")) gbc.anchor = GridBagConstraints.LINE_START;
        else if(method.equalsIgnoreCase("FIRST_LINE_START"))
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        else if(method.equalsIgnoreCase("FIRST_LINE_END")) gbc.anchor = GridBagConstraints.FIRST_LINE_END;

    }
}
