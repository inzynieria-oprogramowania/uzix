package GUI.Panels;

import Communication.GUI_Stream;
import Control.User;
import Exceptions.InitializationException;
import Listeners.Com_Listener;
import Listeners.Panel_Listener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ToolbarPanel extends JPanel implements ActionListener {
    private Panel_Listener listener;
    private GUI_Stream gui_stream;
    private Com_Listener com_listener;
    private JLabel user_label;
    private JButton message_button;
    private JButton log_out_button;

    // pass com listener reference
    private void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener=listener;
    }

    // pass gui stream reference
    private void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    // pass panel listener reference
    private void pass_panel_listener(Panel_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Panel_Listener in class " + this.getClass().getName());
        else this.listener = listener;
    }

    private void initialize() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEtchedBorder());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;

        log_out_button = new JButton("Log out");
        log_out_button.addActionListener(this);
        log_out_button.setFont(new Font("Arial", Font.PLAIN, 20));
        GUI.GUI.formatLocation(gbc, 2, 0, 1, 1, "FIRST_LINE_END");
        add(log_out_button, gbc);

        message_button = new JButton("Message");
        message_button.addActionListener(this);
        message_button.setFont(new Font("Arial", Font.PLAIN, 20));
        GUI.GUI.formatLocation(gbc, 2, 0, 1, 1, "FIRST_LINE_END");
        gbc.insets = new Insets(0, 0, 0, 105);
        add(message_button, gbc);

        user_label = new JLabel("Email: " + User.getEmail());
        user_label.setFont(new Font("Arial", Font.PLAIN, 20));
        GUI.GUI.formatLocation(gbc, 2, 0, 1, 1, "FIRST_LINE_START");
        gbc.insets = new Insets(5, 10, 0, 0);
        add(user_label, gbc);
    }

    // perform panel setup processes
    public final void setup(Com_Listener com, GUI_Stream gui, Panel_Listener listener) {
        try {
            pass_Com_Listener(com);
            pass_GUI_Stream(gui);
            pass_panel_listener(listener);
        } catch (InitializationException e) {
            e.printStackTrace();
        } finally {
            initialize();
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        // TODO: add action performers
    }
}
