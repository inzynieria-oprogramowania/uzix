package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import java.awt.*;
import java.awt.event.ActionEvent;

import static GUI.GUI.formatLocation;

public class DefaultPanel extends WindowPanel {

    public DefaultPanel() { }

    @Override
    public void initialize()
    {
        Label title = new Label("UZIX");
        Label subtitle = new Label("Main Menu");
        GridBagConstraints grid = new GridBagConstraints();
        this.setLayout(new GridBagLayout());

        title.setFont(new Font("SANS_SERIF", Font.ITALIC, 100));
        subtitle.setFont(new Font("SERIF", Font.BOLD, 60));

        grid.fill = GridBagConstraints.NONE;
        formatLocation(grid, 0, 1, 1, 1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 10);
        this.add(title, grid);

        formatLocation(grid, 1, 1, 1, 1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(new Label(), grid);

        formatLocation(grid, 0, 2, 0, 1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, -30);
        this.add(subtitle, grid);

        formatLocation(grid, 1, 2, 1, 1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(new Label(), grid);

        formatLocation(grid, 0, 4, 1, 2.0, "FIRST_LINE_START");
        add(new Label(), grid);
    }

    public void actionPerformed(ActionEvent e) {

    }
}
