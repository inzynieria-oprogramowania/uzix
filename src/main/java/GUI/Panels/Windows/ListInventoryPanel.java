package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;

import static GUI.GUI.formatLocation;

public class ListInventoryPanel extends WindowPanel {
    public void initialize(){
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[] { "ID","Name", "Amount in stock", "Price for unit"});

        // dane tymczasowe

        model.addRow(new Object[]{1,"cos","1000","300"});
        model.addRow(new Object[]{2,"cos2","200","100"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        //end

        JLabel name = new JLabel("List Inventory");
        JTable table = new JTable(model);
        JScrollPane j = new JScrollPane(table);
        j.setVisible(true);

        name.setFont(new Font("Serif", Font.BOLD, 30));

        this.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();
        formatLocation(grid, 0, 0, 0.5, 0, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 150);
        this.add(name, grid);
        formatLocation(grid, 1, 0, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(new Label(), grid);
        formatLocation(grid, 0, 1, 1, 0, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(j, grid);
        formatLocation(grid, 0, 9, 1, 2.0, "FIRST_LINE_START");
        add(new Label(), grid);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
