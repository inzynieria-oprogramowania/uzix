package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import static GUI.GUI.formatLocation;


public class ProfileInfoPanel extends WindowPanel {
    public void initialize()  {


        //tymczasowe dane
        String userName = "Patryk";
        String userSurname = "Migdalski";
        String userDepartment = "asda";
        String userJobTitle = "asda";
        String userEmail = "asda";
        String userPhone = "asda";
        int userVacation = 10;

        // koniec


        URL url = null;
        try{
            url = new URL("https://urzadzamy.smcloud.net/t/photos/t/77298/chomik_3343633.jpg");  // tymczasowo
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.setLayout(new GridBagLayout());
        JLabel name = new JLabel("Name:");
        JLabel nameTextField = new JLabel(userName);
        JLabel surname = new JLabel("Surname:");
        JLabel surnameTextField = new JLabel(userSurname);
        JLabel department = new JLabel("Department:" );
        JLabel departmentTextField = new JLabel(userDepartment);
        JLabel jobTitle = new JLabel("Job Title:");
        JLabel jobTitleTextFiled = new JLabel(userJobTitle);
        JLabel email = new JLabel("E-mail:");
        JLabel emailTextFiled = new JLabel(userEmail);
        JLabel phone = new JLabel("Phone Number:");
        JLabel phoneTextFiled = new JLabel(userPhone);
        JLabel vacation = new JLabel("Vacation days left:");
        JLabel vacationTextFiled = new JLabel(String.valueOf(userVacation));


        ImageIcon icon = null;
        try {
            assert url != null;
            icon = new ImageIcon(ImageIO.read(url).getScaledInstance(100, 150, Image.SCALE_SMOOTH));

        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel image = new JLabel(icon);

        this.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();

        grid.fill = GridBagConstraints.NONE;
        formatLocation(grid, 0, 0, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, -30);
        add(image, grid);
        formatLocation(grid, 0, 1, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(name, grid);
        formatLocation(grid, 1, 1, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(nameTextField, grid);
        formatLocation(grid, 0, 2, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(surname, grid);
        formatLocation(grid, 1, 2, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(surnameTextField, grid);
        formatLocation(grid, 0, 3, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(department, grid);
        formatLocation(grid, 1, 3, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(departmentTextField, grid);
        formatLocation(grid, 0, 4, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(jobTitle, grid);
        formatLocation(grid, 1, 4, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(jobTitleTextFiled, grid);
        formatLocation(grid, 0, 5, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(email, grid);
        formatLocation(grid, 1, 5, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(emailTextFiled, grid);
        formatLocation(grid, 0, 6, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(phone, grid);
        formatLocation(grid, 1, 6, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(phoneTextFiled, grid);
        formatLocation(grid, 0, 7, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(vacation, grid);
        formatLocation(grid, 1, 7, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 25);
        add(vacationTextFiled, grid);

        formatLocation(grid, 0, 8, 1, 2.0, "FIRST_LINE_START");
        add(new Label(), grid);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
