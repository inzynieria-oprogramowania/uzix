package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static GUI.GUI.formatLocation;

public class ProfileEditProfilePanel extends WindowPanel {
    public void initialize()  {


        //tymczasowe dane
        String userName = "Patryk";
        String userSurname = "Migdalski";
        String userDepartment = "asda";
        String userJobTitle = "asda";
        String userEmail = "asda";
        String userPhone = "asda";
        int userVacation = 10;

        // koniec



        URL url = null;
        try{
            url = new URL("https://urzadzamy.smcloud.net/t/photos/t/77298/chomik_3343633.jpg");  // tymczasowo
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.setLayout(new GridBagLayout());
        JLabel name = new JLabel("Name:");
        JTextField nameTextField = new JTextField(8);
        JLabel surname = new JLabel("Surname:");
        JTextField surnameTextField = new JTextField(8);
        JLabel department = new JLabel("Department:" );
        JTextField departmentTextField = new JTextField(8);
        JLabel jobTitle = new JLabel("Job Title:");
        JTextField jobTitleTextFiled = new JTextField(8);
        JLabel email = new JLabel("E-mail:");
        JTextField emailTextFiled = new JTextField(8);
        JLabel phone = new JLabel("Phone Number:");
        JTextField phoneTextFiled = new JTextField(8);
        JLabel vacation = new JLabel("Vacation days left:");
        JLabel vacationTextFiled = new JLabel(String.valueOf(userVacation));
        JButton change = new JButton("Submit changes");

        nameTextField.setText(userName);
        surnameTextField.setText(userSurname);
        departmentTextField.setText(userDepartment);
        jobTitleTextFiled.setText(userJobTitle);
        emailTextFiled.setText(userEmail);
        phoneTextFiled.setText(userPhone);


        ImageIcon icon = null;
        try {
            assert url != null;
            icon = new ImageIcon(ImageIO.read(url).getScaledInstance(100, 150, Image.SCALE_SMOOTH));

        } catch (IOException e) {
            e.printStackTrace();
        }
        JLabel image = new JLabel(icon);

        this.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();

        grid.fill = GridBagConstraints.NONE;
        formatLocation(grid, 0, 0, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, -30);
        add(image, grid);
        formatLocation(grid, 0, 1, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(name, grid);
        formatLocation(grid, 1, 1, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(nameTextField, grid);
        formatLocation(grid, 0, 2, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(surname, grid);
        formatLocation(grid, 1, 2, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(surnameTextField, grid);
        formatLocation(grid, 0, 3, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(department, grid);
        formatLocation(grid, 1, 3, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(departmentTextField, grid);
        formatLocation(grid, 0, 4, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(jobTitle, grid);
        formatLocation(grid, 1, 4, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(jobTitleTextFiled, grid);
        formatLocation(grid, 0, 5, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(email, grid);
        formatLocation(grid, 1, 5, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(emailTextFiled, grid);
        formatLocation(grid, 0, 6, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(phone, grid);
        formatLocation(grid, 1, 6, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(phoneTextFiled, grid);
        formatLocation(grid, 0, 7, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 30);
        add(vacation, grid);
        formatLocation(grid, 1, 7, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        add(vacationTextFiled, grid);
        formatLocation(grid, 0, 8, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, -50);
        add(change, grid);
        formatLocation(grid, 0, 9, 1, 2.0, "FIRST_LINE_START");
        add(new Label(), grid);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
