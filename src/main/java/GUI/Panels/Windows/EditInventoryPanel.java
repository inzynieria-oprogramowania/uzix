package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static GUI.GUI.formatLocation;

public class EditInventoryPanel extends WindowPanel {
    @Override
    public void initialize(){
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[] { "ID","Name", "Amount in stock", "Price for unit"});

        // dane tymczasowe

        model.addRow(new Object[]{1,"cos","1000","300"});
        model.addRow(new Object[]{2,"cos2","200","100"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        model.addRow(new Object[]{3,"cos3","300","200"});
        //end

        JLabel name = new JLabel("List Inventory");
        JTable table = new JTable(model);
        JScrollPane j = new JScrollPane(table);
        j.setVisible(true);

        name.setFont(new Font("Serif", Font.BOLD, 30));

        JTextField idTable = new JTextField(5);
        JTextField nameTable  = new JTextField(5);
        JTextField amountTable  = new JTextField(5);
        JTextField priceTable  = new JTextField(5);
        JButton submit = new JButton("Submit");
        idTable.setEditable(false);

        this.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();
        formatLocation(grid, 0, 0, 0.5, 0, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 150);
        this.add(name, grid);
        formatLocation(grid, 1, 0, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(new Label(), grid);
        formatLocation(grid, 0, 1, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(j, grid);
        formatLocation(grid, 1, 1, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(new Label(), grid);
        /**/
        formatLocation(grid, 0, 2, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 220);
        this.add(new Label("ID:"), grid);

        formatLocation(grid, 1, 2, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -220, 0, 0);
        this.add(idTable, grid);

        formatLocation(grid, 0, 3, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 220);
        this.add(new Label("Name:"), grid);


        formatLocation(grid, 1, 3, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -220, 0, 0);
        this.add(nameTable, grid);

        formatLocation(grid, 0, 4, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 220);
        this.add(new Label("Amount in stock:"), grid);


        formatLocation(grid, 1, 4, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -220, 0, 0);
        this.add(amountTable, grid);

        formatLocation(grid, 0, 5, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 220);
        this.add(new Label("Price for unit:"), grid);

        formatLocation(grid, 1, 5, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -220, 0, 0);
        this.add(priceTable, grid);

        formatLocation(grid, 0, 6, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 200);
        this.add(submit, grid);

        formatLocation(grid, 1, 6, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, 0, 0, 220);
        this.add(new JLabel(), grid);

        formatLocation(grid, 0, 7, 1, 2.0, "FIRST_LINE_START");
        add(new JLabel(), grid);



        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                Point p = me.getPoint();
                int row = table.rowAtPoint(p);
                if (me.getClickCount() == 2) {

                    for(int i=0;i<4;i++)
                    {
                        switch (selectedColumn(table.getColumnName(i)))
                        {
                            case 0:
                                idTable.setText(table.getValueAt(row,0).toString());
                                break;
                            case 1:
                                nameTable.setText(table.getValueAt(row,1).toString());
                                break;
                            case 2:
                                amountTable.setText(table.getValueAt(row,2).toString());
                                break;
                            case 3:
                                priceTable.setText(table.getValueAt(row,3).toString());
                        }
                    }


                }
            }
            public int selectedColumn(String name)
            {
                switch (name)
                {
                    case "Id":
                        return  0;
                    case "Name":
                        return 1;
                    case "Amount in stock":
                        return 2;
                    case "Price for unit":
                        return 3;
                }
                return 0;
            }

        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
