package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;

import static GUI.GUI.formatLocation;

public class InventoryReportPanel extends WindowPanel {
    @Override
    public void initialize() {
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[] { "ID","Product Type", "Model","Amount in stock", "Price"});
        JLabel totalItem = new JLabel();
        JLabel totalPrice = new JLabel();
        JButton report = new JButton("Download Report");

        // dane tymczasowe
        totalItem.setText(String.valueOf(5000)); //powinno być zliczane za pomocą sql
        totalPrice.setText(String.valueOf(100000));

        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        model.addRow(new Object[]{1,"cos","asd","300","111"});
        //end


        JTable table = new JTable(model);
        JScrollPane j = new JScrollPane(table);
        j.setVisible(true);

        this.setLayout(new GridBagLayout());

        GridBagConstraints grid = new GridBagConstraints();
        formatLocation(grid, 0, 0, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 230);
        this.add(new JLabel("Total number of items: "), grid);

        formatLocation(grid, 1, 0, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -200, 0, 0);
        this.add(totalItem, grid);

        formatLocation(grid, 0, 1, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 230);
        this.add(new JLabel("Total price: "), grid);

        formatLocation(grid, 1, 1, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -200, 0, 0);
        this.add(totalPrice, grid);

        formatLocation(grid, 0, 2, 1, 0.1, "LINE_END");
        grid.insets = new Insets(0, 0, 10, 0);
        this.add(report, grid);

        formatLocation(grid, 2, 1, 1, 0.1, "LINE_START");
        grid.insets = new Insets(0, -200, 10, 0);
        this.add(new JLabel(), grid);

        formatLocation(grid, 0, 3, 1, 0, "LINE_END");
        grid.insets = new Insets(0, 0, 0, 0);
        this.add(j, grid);

        formatLocation(grid, 0, 4, 1, 2.0, "FIRST_LINE_START");
        add(new Label(), grid);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
