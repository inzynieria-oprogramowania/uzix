package GUI.Panels.Windows;

import GUI.Panels.General.WindowPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;


public class CoworkersListPanel extends WindowPanel {

    public void initialize() {
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[] { "ID","Name", "Surname", "Job", "Department"});

        // dane tymczasowe

        model.addRow(new Object[]{1,"Patryk","Migdalski","dsa","ada"});
        model.addRow(new Object[]{2,"Patryk","Migdalski","dsa","ada"});
        model.addRow(new Object[]{3,"Patryk","Migdalski","dsa","ada"});
        //end

        JTable table = new JTable(model);
        JScrollPane j = new JScrollPane(table);
        j.setVisible(true);
        this.add(j);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }
}

