package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class InventoryMenu extends MenuPanel {
    private JButton list_inventory;
    private JButton search_inventory;
    private JButton inventory_detail;
    private JButton edit_inventory;
    private JButton back;

    protected void initialize() {
        list_inventory = new JButton("List Inventory");
        configureButton(list_inventory);

        search_inventory = new JButton("Search Inventory");
        configureButton(search_inventory);

        inventory_detail = new JButton("Inventory Detail");
        configureButton(inventory_detail);

        edit_inventory = new JButton("Edit Inventory");
        configureButton(edit_inventory);

        back = new JButton("Back");
        configureButton(back);
    }

    // TODO: usunąć przejścia testowe i włączyć prawidłowe przejścia
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == list_inventory) {
            listener.emit_Update_Request(PanelType.ListInventoryPanel, PanelType.InventoryMenu);
        } else if (actionEvent.getSource() == search_inventory) {
            //listener.emit_Update_Request(PanelType.SearchInventoryPanel, PanelType.InventoryMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.InventoryMenu);
        } else if (actionEvent.getSource() == inventory_detail) {
            //listener.emit_Update_Request(PanelType.InventoryDetailPanel, PanelType.InventoryMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.InventoryMenu);
        }else if (actionEvent.getSource() == edit_inventory) {
            listener.emit_Update_Request(PanelType.EditInventoryPanel, PanelType.InventoryMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
