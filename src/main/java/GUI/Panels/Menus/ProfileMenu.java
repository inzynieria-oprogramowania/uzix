package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ProfileMenu extends MenuPanel {
    private JButton my_profile;
    private JButton change_password;
    private JButton settings;
    private JButton back;

    protected void initialize() {
        my_profile = new JButton("My Profile");
        configureButton(my_profile);

        change_password = new JButton("Change Password");
        configureButton(change_password);

        settings = new JButton("Settings");
        configureButton(settings);

        back = new JButton("Back");
        configureButton(back);
    }

    // TODO: usunąć przejścia testowe i włączyć prawidłowe przejścia
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == my_profile) {
            listener.emit_Update_Request(PanelType.ProfileInfoPanel, PanelType.ProfileMenu);
        } else if (actionEvent.getSource() == change_password) {
            //listener.emit_Update_Request(PanelType.ProfilePasswordPanel, PanelType.ProfileMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.ProfileMenu);
        } else if (actionEvent.getSource() == settings) {
            listener.emit_Update_Request(PanelType.ProfileEditProfilePanel, PanelType.ProfileMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
