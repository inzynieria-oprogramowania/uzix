package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class OrdersMenu extends MenuPanel {
    private JButton make_order;
    private JButton check_orders;
    private JButton edit_orders_search;
    private JButton edit_orders_info;
    private JButton back;

    protected void initialize() {
        make_order = new JButton("Make Order");
        configureButton(make_order);

        check_orders = new JButton("Check Orders");
        configureButton(check_orders);

        edit_orders_search = new JButton("Edit Orders Search");
        configureButton(edit_orders_search);

        edit_orders_info = new JButton("Edit Orders Info");
        configureButton(edit_orders_info);

        back = new JButton("Back");
        configureButton(back);
    }

    // TODO: usunąć przejścia testowe i włączyć prawidłowe przejścia
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == make_order) {
            //listener.emit_Update_Request(PanelType.MakeOrderPanel, PanelType.OrdersMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.OrdersMenu);
        } else if (actionEvent.getSource() == check_orders) {
            //listener.emit_Update_Request(PanelType.CheckOrdersPanel, PanelType.OrdersMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.OrdersMenu);
        } else if (actionEvent.getSource() == edit_orders_search) {
            //listener.emit_Update_Request(PanelType.EditOrderSearchPanel, PanelType.OrdersMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.OrdersMenu);
        } else if (actionEvent.getSource() == edit_orders_info) {
            //listener.emit_Update_Request(PanelType.EditOrderInfoPanel, PanelType.OrdersMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.OrdersMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
