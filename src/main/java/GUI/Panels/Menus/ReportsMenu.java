package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ReportsMenu extends MenuPanel {
    private JButton inventory_report;
    private JButton order_report;
    private JButton financial_report;
    private JButton back;

    protected void initialize() {
        inventory_report = new JButton("Inventory Report");
        configureButton(inventory_report);

        order_report = new JButton("Order Report");
        configureButton(order_report);

        financial_report = new JButton("Financial Report");
        configureButton(financial_report);

        back = new JButton("Back");
        configureButton(back);
    }

    // TODO: usunąć przejścia testowe i włączyć prawidłowe przejścia
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == inventory_report) {
            listener.emit_Update_Request(PanelType.InventoryReportPanel, PanelType.ReportsMenu);
        } else if (actionEvent.getSource() == order_report) {
            listener.emit_Update_Request(PanelType.OrderReportPanel, PanelType.ReportsMenu);
        } else if (actionEvent.getSource() == financial_report) {
            //listener.emit_Update_Request(PanelType.FinancialReportPanel, PanelType.ReportsMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.ReportsMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
