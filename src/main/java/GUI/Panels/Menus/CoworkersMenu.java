package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CoworkersMenu extends MenuPanel {
    private JButton coworkers_list;
    private JButton coworkers_detail;
    private JButton back;

    protected void initialize() {
        coworkers_list = new JButton("Coworkers List");
        configureButton(coworkers_list);

        coworkers_detail = new JButton("Coworkers Detail");
        configureButton(coworkers_detail);

        back = new JButton("Back");
        configureButton(back);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == coworkers_list) {
            listener.emit_Update_Request(PanelType.CoworkersListPanel, PanelType.CoworkersMenu);
        } else if (actionEvent.getSource() == coworkers_detail) {
            //listener.emit_Update_Request(PanelType.CoworkersDetailPanel, PanelType.CoworkersMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.CoworkersMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
