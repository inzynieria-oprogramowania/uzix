package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class AdminMenu extends MenuPanel {
    private JButton admin_add_user;
    private JButton admin_edit_user;
    private JButton admin_delete_user;
    private JButton back;

    protected void initialize() {
        admin_add_user = new JButton("Add User");
        configureButton(admin_add_user);

        admin_edit_user = new JButton("Edit User");
        configureButton(admin_edit_user);

        admin_delete_user = new JButton("Delete User");
        configureButton(admin_delete_user);

        back = new JButton("Back");
        configureButton(back);
    }

    // TODO: usunąć przejścia testowe i włączyć prawidłowe przejścia
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == admin_add_user) {
            //listener.emit_Update_Request(PanelType.AdminAddUserPanel, PanelType.AdminMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.AdminMenu);
        } else if (actionEvent.getSource() == admin_edit_user) {
            //listener.emit_Update_Request(PanelType.AdminEditUserPanel, PanelType.AdminMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.AdminMenu);
        } else if (actionEvent.getSource() == admin_delete_user) {
            //listener.emit_Update_Request(PanelType.AdminDeleteUserPanel, PanelType.AdminMenu);
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.AdminMenu);
        } else if (actionEvent.getSource() == back) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        }
    }
}
