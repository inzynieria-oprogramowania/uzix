package GUI.Panels.Menus;

import GUI.PanelType;
import GUI.Panels.General.MenuPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class MainMenu extends MenuPanel {
    private JButton profile_button;
    private JButton orders_button;
    private JButton inventory_button;
    private JButton reports_button;
    private JButton coworkers_button;
    private JButton maintenance_button;
    private JButton admin_button;

    protected void initialize() {
        profile_button = new JButton("Profile");
        configureButton(profile_button);

        orders_button = new JButton("Orders");
        configureButton(orders_button);

        inventory_button = new JButton("Inventory");
        configureButton(inventory_button);

        reports_button = new JButton("Reports");
        configureButton(reports_button);

        coworkers_button = new JButton("Coworkers");
        configureButton(coworkers_button);

        maintenance_button = new JButton("Maintenance");
        configureButton(maintenance_button);

        admin_button = new JButton("Administrate");
        configureButton(admin_button);
    }

    // TODO: dokończyć i poprawić
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource() == profile_button) {
            // TODO: dodać przełączanie na prawidłowe panele
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.ProfileMenu);
        } else if (actionEvent.getSource() == orders_button) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.OrdersMenu);
        } else if (actionEvent.getSource() == inventory_button) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.InventoryMenu);
        } else if (actionEvent.getSource() == reports_button) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.ReportsMenu);
        } else if (actionEvent.getSource() == coworkers_button) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.CoworkersMenu);
        } else if (actionEvent.getSource() == maintenance_button) {
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.MainMenu);
        } else if (actionEvent.getSource() == admin_button) {
            // TODO: Dodać weryfikacje bycia administratorem
            listener.emit_Update_Request(PanelType.DefaultPanel, PanelType.AdminMenu);
        }
    }
}
