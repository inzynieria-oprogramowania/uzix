package GUI.Panels.General;

import Communication.GUI_Stream;
import Exceptions.InitializationException;
import Listeners.Com_Listener;
import Listeners.Panel_Listener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public abstract class MenuPanel extends JPanel implements ActionListener {
    protected Panel_Listener listener;
    protected GUI_Stream gui_stream;
    protected Com_Listener com_listener;
    protected JPanel box;

    public MenuPanel() {
        // setting panel width
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dim = getPreferredSize();
        dim.width = (int) (screenDim.width * 0.15);
        dim.height = (int) (dim.height * 0.9);
        setPreferredSize(dim);

        // configuring panel
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        box = new JPanel(new GridLayout(7, 1));
        add(box);
    }

    // pass com listener reference
    protected final void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener=listener;
    }

    // pass gui stream reference
    protected final void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    // pass panel listener reference
    protected final void pass_panel_listener(Panel_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Panel_Listener in class " + this.getClass().getName());
        else this.listener = listener;
    }

    // perform panel setup processes
    public final void setup(Com_Listener com, GUI_Stream gui, Panel_Listener listener) {
        try {
            pass_Com_Listener(com);
            pass_GUI_Stream(gui);
            pass_panel_listener(listener);
        } catch (InitializationException e) {
            e.printStackTrace();
        } finally {
            initialize();
        }
    }

    protected abstract void initialize();

    // configure button parameters
    protected void configureButton(JButton button) {
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setFont(new Font("Arial", Font.PLAIN, 20));
        button.addActionListener(this);
        box.add(button);
    }
}
