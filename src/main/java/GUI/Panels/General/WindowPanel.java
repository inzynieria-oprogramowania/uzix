package GUI.Panels.General;

import Communication.GUI_Stream;
import Exceptions.InitializationException;
import Listeners.Com_Listener;
import Listeners.Panel_Listener;

import javax.swing.*;
import java.awt.event.ActionListener;

public abstract class WindowPanel extends JPanel implements ActionListener {
    private Panel_Listener listener;
    private GUI_Stream gui_stream;
    private Com_Listener com_listener;
    private boolean requestAnswered = false;

    // pass com listener reference
    protected final void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener=listener;
    }

    // pass gui stream reference
    protected final void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    // pass panel listener reference
    protected final void pass_panel_listener(Panel_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Panel_Listener in class " + this.getClass().getName());
        else this.listener = listener;
    }

    // perform panel setup processes
    public final void setup(Com_Listener com, GUI_Stream gui, Panel_Listener listener) {
        try {
            pass_Com_Listener(com);
            pass_GUI_Stream(gui);
            pass_panel_listener(listener);
            initialize();
        } catch (InitializationException e) {
            e.printStackTrace();
        }
    }

    // inform panel that request answer is ready
    public final void inform() { requestAnswered = true; }

    protected abstract void initialize();

    public void init_loading() {}
}
