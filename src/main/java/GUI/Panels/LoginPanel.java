package GUI.Panels;

import Communication.GUI_Stream;
import Communication.Message;
import Control.User;
import Exceptions.InitializationException;
import Exceptions.NoContentException;
import Exceptions.NoMessageException;
import Exceptions.StreamNotReservedException;
import Listeners.Com_Listener;
import Listeners.Frame_Listener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPanel extends JPanel implements ActionListener {
    private Frame_Listener frame_listener;
    private Com_Listener com_listener;
    private GUI_Stream gui_stream;
    private JPasswordField password_Field;
    private JTextField login_TextField;
    private JLabel wrong;
    private boolean requestAnswered;

    public LoginPanel() { requestAnswered = false; }

    // passing com listener reference
    public void pass_Com_Listener(Com_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Com_Listener in class " + this.getClass().getName());
        else com_listener = listener;
    }

    // passing gui stream reference
    public void pass_GUI_Stream(GUI_Stream stream) throws InitializationException {
        if(stream == null)
            throw new InitializationException("Received null as GUI_Stream in class " + this.getClass().getName());
        else gui_stream = stream;
    }

    // passing frame listener reference
    public void pass_LF_Listener(Frame_Listener listener) throws InitializationException {
        if(listener == null)
            throw new InitializationException("Received null as Frame_Listener in class " + this.getClass().getName());
        else frame_listener = listener;
    }

    public void initialize() {
        // create GUI elements
        JButton sign_in_button = new JButton("Sign In");
        JLabel login_Label = new JLabel("Login: ");
        JLabel password_Label = new JLabel("Password: ");
        wrong = new JLabel("Wrong login or password");
        login_TextField = new JTextField(30);
        password_Field = new JPasswordField(30);

        // set border and layout
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new GridBagLayout());

        // format elements positions and add to layout
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;

        // for login label
        GUI.GUI.formatLocation(gbc, 0, 0, 1, 0.1, "LINE_END");
        gbc.insets = new Insets(0, 0, 0, 5);
        add(login_Label, gbc);

        // for login textfield
        GUI.GUI.formatLocation(gbc, 1, 0, 1, 0.1, "LINE_START");
        gbc.insets = new Insets(0, 0, 0, 0);
        add(login_TextField, gbc);

        // for password label
        GUI.GUI.formatLocation(gbc, 0, 1, 1, 0.1, "LINE_END");
        gbc.insets = new Insets(0, 0, 0, 5);
        add(password_Label, gbc);

        // for password field
        GUI.GUI.formatLocation(gbc, 1, 1, 1, 0.1, "LINE_START");
        gbc.insets = new Insets(0, 0, 0, 0);
        add(password_Field, gbc);

        // for sign in button
        GUI.GUI.formatLocation(gbc, 1, 3, 1, 2.0, "FIRST_LINE_START");
        add(sign_in_button, gbc);

        // for wrong credentials message
        GUI.GUI.formatLocation(gbc, 1, 3, 0, 3.1, "FIRST_LINE_START");
        gbc.insets = new Insets(0, 100, 0,0);
        add(wrong, gbc);

        // hide wrong credentials message by default
        wrong.setVisible(false);

        // format button attributes
        sign_in_button.addActionListener(this);
    }

    // verification of credentials
    public boolean verify() {
        while(!requestAnswered) System.out.print("");
        try {
            System.out.println("LoginPanel was informed");
            return gui_stream.receive().content.get(0).equalsIgnoreCase("OK");
        } catch (NoMessageException | StreamNotReservedException e) {
            e.printStackTrace();
            return false;
        } finally {
            requestAnswered = false;
        }
    }

    // inform panel that answer is ready to read
    public void inform() {
        System.out.println("Login panel information received");
        requestAnswered = true; }

    // action listener for sign in button
    public void actionPerformed(ActionEvent e) {
        // sending credentials to com module
        System.out.println("Button press detected");
        Message message = new Message();
        message.sender = "LoginPanel";
        message.receiver = "Com_Module";
        message.content.add(login_TextField.getText());
        StringBuilder password = new StringBuilder();
        for(char c : password_Field.getPassword()) {
            password.append(c);
        }
        message.content.add(password.toString());
        gui_stream.acquire();
        System.out.println("Stream acquired");
        try {
            gui_stream.send(message);
        } catch (NoContentException | StreamNotReservedException ex) {
            ex.printStackTrace();
        }
        com_listener.emit_Request();

        // procedure after verifying credentials
        System.out.println("Request sent");
        if(verify()) {
            System.out.println("Verified successfully");
            password_Field.setText("");
            User.setUsername(login_TextField.getText());
            /*try {
                // TODO: Wysłać zapytanie o e-mail użytkownika
                User.setEmail(gui_stream.receive().content.get(0));
            } catch (NoMessageException | StreamNotReservedException ex) {
                ex.printStackTrace();
            }*/
            try {
                gui_stream.release();
            } catch (StreamNotReservedException ex) {
                ex.printStackTrace();
            }
            frame_listener.trigger_swap();
        } else {
            System.out.println("Failed to verify");
            wrong.setVisible(true);
        }
    }
}
