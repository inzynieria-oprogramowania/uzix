package GUI;

public enum PanelType {
    MainMenu, ProfileMenu, CoworkersMenu, OrdersMenu, InventoryMenu, ReportsMenu, DefaultMenu,
    AdminMenu,
    ProfileInfoPanel, ProfileEditProfilePanel, ProfilePasswordPanel,
    MakeOrderPanel, CheckOrdersPanel, EditOrderSearchPanel, EditOrderInfoPanel,
    ListInventoryPanel, SearchInventoryPanel, InventoryDetailPanel, EditInventoryPanel,
    InventoryReportPanel, OrderReportPanel, FinancialReportPanel,
    CoworkersListPanel, CoworkersDetailPanel,
    AdminAddUserPanel, AdminEditUserPanel, AdminDeleteUserPanel,
    DefaultPanel
}
