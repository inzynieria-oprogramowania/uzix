import javax.swing.*;

public class Application {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Program program = new Program();
            program.start();
            try {
                program.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
