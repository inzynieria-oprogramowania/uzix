package Listeners;

import GUI.PanelType;

public interface Panel_Listener {
    void emit_Update_Request(PanelType window_panel, PanelType menu_panel);
}
