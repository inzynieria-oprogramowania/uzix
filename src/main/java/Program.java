import Communication.Com_Module;
import Communication.GUI_Stream;
import Control.Log;
import Exceptions.InitializationException;
import GUI.GUI;

import java.io.IOException;
import java.util.Arrays;

public class Program extends Thread {
    private GUI gui;
    private Com_Module communication;
    private GUI_Stream gui_stream;

    public Program() {
        try {
            Log.start();
            create_Stream();
            init_GUI();
            init_Communication();
            Log.write("Program fully initialized");
        } catch (InitializationException | IOException e) {
            e.printStackTrace();
            Log.write("Program initialization error");
            Log.write(Arrays.toString(e.getStackTrace()));
            System.exit(1);
        }
    }

    private void create_Stream() {
        gui_stream = new GUI_Stream();
        Log.write("GUI_Stream created");
    }

    private void init_Communication() throws InitializationException {
        communication = new Com_Module();
        try {
            communication.pass_GUI_Stream(gui_stream);
            Log.write("GUI_Stream passed to Com_Module");
            communication.pass_GUI_Listener((String target) -> gui.inform(target));
            Log.write("Signal exchange between Com_Module and GUI enabled");
        } catch (InitializationException e) {
            e.printStackTrace();
            throw e;
        }
        communication.start();
        Log.write("Com_Module launched");
    }

    private void init_GUI() throws InitializationException {
        gui = new GUI();
        try {
            gui.set_Com_Listener(() -> communication.receive_request());
            Log.write("Com_Listener passed to GUI");
            gui.set_GUI_Stream(gui_stream);
            Log.write("GUI_Stream passed to GUI");
            gui.initialize();
            Log.write("GUI fully initialized");
        } catch (InitializationException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
